
  const first= (inventory,carId)=>{

    if(!Number.isInteger(carId)){
        //console.log("Number error")
        return []
    }

    if(!inventory ){
        //console.log("inventory empty error")
        return []
    }


    if(inventory.length===0){
        //console.log("inventory length error")
        return []
    }

    if(!Array.isArray(inventory)){
        //console.log("inventory type error")
        return []
    }


    for(let car=0;car<inventory.length;car++){

        if(inventory[car].id === carId){

            let values = inventory[car]
            console.log(`Car ${carId} is a ${values.car_year} ${values.car_make} ${values.car_model}`)
            return values
        }
    }

    return []
}

module.exports = first
const problem1 = require("../problem1.cjs")
const inventory = require("../inventory.cjs")

console.log(problem1())
console.log(problem1(new String("hello"), 3))
console.log(problem1([{"id":0,"car_make":"Dodge","car_model":"Magnum","car_year":2008}], 0))
console.log(problem1({id: 33, name: "Test", length: 10}, ""))

console.log(problem1(inventory, 33))
